require 'matrix'
class MyMatrix
    # code
    attr_reader :score
    # score = '1,2|3,4'
    def initialize(argument)
        @score = argument
    end

    def convert
        ara = @score.split('|')
        ara.map { |e| e.split(',').map(&:to_i) }
    end

    def tran
        return 'Array is empty' if @score.empty?
        Matrix[*convert].transpose.to_a
    rescue ExceptionForMatrix::ErrDimensionMismatch
        return 'ErrDimensionMismatch'
    end
end
