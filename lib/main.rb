require 'sinatra'
require_relative 'mymatrix'

get '/' do
    erb :index, layout: false do
        erb :input
    end
end

get '/result' do
    redirect to('/')
end

post '/result' do
    erb :index, layout: false do
        @ara = MyMatrix.new(params['string']).tran
        if @ara.is_a?(Array)
            erb :result
        else
            erb :error
        end
    end
end
