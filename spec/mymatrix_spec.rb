require 'rspec'
require 'mymatrix'
require 'matrix'
RSpec.describe Matrix do
    test_str = '1,2|3,4'
    qua = MyMatrix.new(test_str)
    it 'Get string' do
        expect(qua.score).to eq test_str
    end
    it 'Shoule convert to array' do
        expect(qua.convert).to eq [[1, 2], [3, 4]]
    end
    it 'Should tran array' do
        expect(qua.tran).to eq [[1, 3], [2, 4]]
    end

    it 'Should transpose non square matrix' do
        test_str = '1,2,3|3,4,5'
        qua = MyMatrix.new(test_str)
        expect(qua.tran).to eq [[1, 3], [2, 4], [3, 5]]
    end
    it 'Incorrect input' do
        test_str = '1,2,3|3,5'
        qua = MyMatrix.new(test_str)
        expect(qua.tran).to eq 'ErrDimensionMismatch'
        test_str = '1,2,3||3,5'
        qua = MyMatrix.new(test_str)
        expect(qua.tran).to eq 'ErrDimensionMismatch'
    end
    it 'Empty array' do
        test_str = ''
        qua = MyMatrix.new(test_str)
        expect(qua.tran).to eq 'Array is empty'
    end
end
